---
layout: page
title: 朋友和收藏
permalink: /friends/
---

 - [walterlv https://walterlv.github.io/](https://walterlv.github.io/ ) 

   这是我师傅

 - [泰琪 https://gandalfliang.github.io/](https://gandalfliang.github.io/)

   对d3d有很深的研究

 - [李继龙 https://kljzndx.github.io/My-Blog/](https://kljzndx.github.io/My-Blog/)

   传说中的开心开发者

 - [陈浩翔 http://chenhaoxiang.cn/](http://chenhaoxiang.cn/ )

 - [dotnet职业技术学院](https://dotnet-campus.github.io/)

   这是我所在的组织

 - [晒太阳的猫 http://jasongrass.gitee.io/ ](http://jasongrass.gitee.io/ ) 

   WPF开发大神

 - [niuyanjie's blog 一个菜鸟的成长历程](http://niuyanjie.gitee.io/blog/ ) 

   WPF开发大神

 - [Nero\`s Blog](http://erdao123.gitee.io/nero/ )  

   有很多单元测试博客

 - [Miss_Bread的博客 - CSDN博客](http://blog.csdn.net/miss_bread )

   我们组的女装程序员  

 - [The Terminal - 博客园](http://www.cnblogs.com/pengzhong )
 
   驸马爷，一个做前端的wpf大神

 - [黄腾霄 ](https://huangtengxiao.gitee.io/ )    

   头像大神，wpf大神

 - [wolfleave ](https://wolfleave.github.io/ )

   传说的幸斌师兄

 - [Nullptr](http://blog.nullptr.top/ )  

 - [杨宇杰 https://okcthouder.github.io/](https://okcthouder.github.io/)

 - [编程新手的小站 http://17i648w554.iask.in/wordpress/](http://17i648w554.iask.in/wordpress/)

 - [极简天气UWP开发者博客 http://fionlen.azurewebsites.net/](http://fionlen.azurewebsites.net/)

 - [张高兴 http://www.cnblogs.com/zhanggaoxing/default.html](http://www.cnblogs.com/zhanggaoxing/default.html?page=1)

   UWP IOT 大神

 - [追梦园 http://www.zmy123.cn](http://www.zmy123.cn)

   里面有很多uwp的博客

 - [wblearn https://wblearn.github.io/](https://wblearn.github.io/)

 - [吾勇士 http://wuyongshi.top/pages/1479085886341.html](http://wuyongshi.top/pages/1479085886341.html)

 - [frendguo's blog  Talk is cheap, show me the code!!!](http://frendguo.top/ )

 - [Leonn https://liyuans.com](https://liyuans.com)

 - [(/≧▽≦/)咦！又好了！ Dreamwings – 继续踏上旅途，在没有你的春天……](https://www.dreamwings.cn/ )

  - [老李拉面馆](http://wicrosoft.ml/ ) UWP 大神

   千千，一个可爱的蓝孩子

 - [YOYOFx](http://blog.microservice4.net/  ) 

   Asp大神，研究.net core

 - [XNA(MonoGame)游戏开发 – 用C#开发跨平台游戏](https://www.xnadevelop.com/ )

   大王，不解释他是谁

 - [迪莫的小站](http://www.dimojang.com/ )  

 - [五斤](https://www.chairyfish.com/ )

 - [小文's blog - 小文的个人博客](https://www.qcgzxw.cn/ )

 - [Moe](https://sunnycase.moe/ ) 

   WPF 大神
  
 - [火火](https://blog.ultrabluefire.cn/ ) UWP 大神

 - [LiesAuer](http://www.liesauer.net/blog/ )  

 - [陈计节的博客](https://blog.jijiechen.com/post/ ) 
 
   dotnet core 大神 

 - [一起帮](http://17bang.ren/ )  

   一对一远程互助平台，有什么问题可以在这里问

 - [我是卧底](https://www.songshizhao.com/blog/blogPage/507.html )  

## 国内博客

 - [【WinRT】国内外 Windows 应用商店应用开发者博客收集 - h82258652 - 博客园](http://www.cnblogs.com/h82258652/p/4909957.html)

 - [vscode使用笔记 - 木杉的博客](http://mushanshitiancai.github.io/2017/01/07/tools/vscode%E4%BD%BF%E7%94%A8%E7%AC%94%E8%AE%B0/)

 - [东邪独孤 - 博客园](http://www.cnblogs.com/tcjiaan/ ) 
   
   老周，买了他的《Windows 10 应用开发实战》写的很好，入门使用这本书对我帮助很大。只是代码没用，因为是代码是uap的，不过修改难度小。老周在极客学院上录制了 Windows10 应用开发的教学视频：http://my.jikexueyuan.com/1795437896/record/ 他的视频也是国内仅有的几个最好的。我很多博客都是参考老周的。

 - [六兆煮橙不会写代码 - CSDN博客](http://blog.csdn.net/lzl1918 )

 - [怪咖先森的博客 - 博客频道 - CSDN.NET](http://blog.csdn.net/u011033906?viewmode=contents)

 - [☜ 我追求的天空 ☞┅┅┅┅┅﹣·☆ - CSDN博客](http://blog.csdn.net/xuzhongxuan )

 - [C_Li_的博客 - CSDN博客](http://blog.csdn.net/github_36704374?viewmode=contents )

 - [hystar - 博客园](http://www.cnblogs.com/lsxqw2004 )
 
 - [周永恒 - 博客园](http://www.cnblogs.com/Zhouyongh )

 - [MS-UAP - 博客园](http://www.cnblogs.com/ms-uap/ )

   微软Windows 工程院的团队博客 邹老师的团队。

 - MSP_甄心cherish http://blog.csdn.net/zmq570235977 

   开始就是看甄心大神的博客。

 - Fantasiax http://blog.csdn.net/fantasiax 

   我的wpf就是在他博客学到的

 - linzheng http://www.cnblogs.com/linzheng/  

   我的wpf就是在他博客学到的

 - 码农很忙 http://www.sum16.com 

 - 王陈染 http://www.wangchenran.com/ 

   最早看到他发的UWP文章

 - zhxilin http://www.cnblogs.com/zhxilin/ 

   礼物说 App 作者。

 - validvoid http://www.cnblogs.com/validvoid/ 

   大量Win2D博客

 - nomasp http://blog.csdn.net/nomasp/ 

   柯于旺大神，现在小米，没有继续做UWP，但他写了CSDN第一个UWP专栏

 - 叔叔的博客 http://www.cnblogs.com/manupstairs/

 - http://blog.higan.me/ msp，二次元 

   写了很多瀑布流博客 

 - msp的昌伟哥哥 http://www.cnblogs.com/mantgh

   刚进微软的大神，他有好多HoloLens文章

 - [姜晔的技术专栏 - CSDN博客](http://blog.csdn.net/ioio_jy?viewmode=contents )

 - [E不小心 - 博客园](http://www.cnblogs.com/gaoshang212 )

 - [叛逆者](https://www.zhihu.com/people/minmin.gong/activities)

   Microsoft Senior SDE（微软高级软件工程师），KlayGE开源游戏引擎创始人

 - [durow - 博客园](http://www.cnblogs.com/durow/ )

 - [法的空间 - 博客园](http://www.cnblogs.com/FaDeKongJian )

 - [浅蓝 - 博客园](http://www.cnblogs.com/qianblue )

 - [youngytj - 博客园](http://www.cnblogs.com/youngytj )

 - [时嬴政 - 博客园](http://www.cnblogs.com/shiyingzheng )

 - [UWPBOX WIN10开发类容分享](http://uwpbox.com/ )

   C++ uwp 大神

 - [bomo - 博客园](http://www.cnblogs.com/bomo )

 - [LH806732的专栏 - CSDN博客](http://blog.csdn.net/LH806732 )

 - [Berumotto - 博客园](http://www.cnblogs.com/KudouShinichi/ )

 - [Edi Wang](http://edi.wang/ ) 国内.net 的大神，有很多的文章，有WPF、UWP还有其他的linux……

 - [尽管扯淡](http://jameszhan.github.io/ ) 写了很多数学

 - [怪咖先森的博客 - CSDN博客](http://blog.csdn.net/u011033906 )

 - [丁校长 - 博客园](http://www.cnblogs.com/dingdaheng )

 - [dino.c](http://www.cnblogs.com/dino623/ ) 写了很多动画

 - [火火](http://www.cnblogs.com/blue-fire/ ) 在有了 Composition API 就很少使用 StoryBoard 的大神

 - [珂珂的个人博客 - 一个程序猿的个人网站](http://kecq.com/)

 - [毛利的技术小站](https://mourinaruto.github.io/?file=Home ) C++ 大神研究 windows 原理

## 国外博客

 - [Font Awesome Icons](http://fontawesome .io/icons/)
 - [Josh Smith](https://joshsmithonwpf.wordpress.com/ )
 - [Dr. WPF](http://drwpf.com/blog/ )
 - http://xamlnative.com/
 - [jamescroft](http://jamescroft.co.uk)  我们在做WinUx，他是微软的大神
 - [Sam Beauvois Home page](http://www.sambeauvois.be/ )
 - [Dany Khalife](http://www.dkhalife.com/ )
 - [Diederik Krols  Short description of the blog](https://blogs.u2u.be/diederik )
 - [Frank's World of Data Science – Data Science for Developers](http://www.franksworld.com/ )
 - [Romasz.net](http://www.romasz.net/ ) wpf 的大神
 - [Jon Skeet's coding blog](https://codeblog.jonskeet.uk/ )
 - [CodeS-SourceS - CCM](http://codes-sources.commentcamarche.net/ )
 - [Igor Kulman](https://blog.kulman.sk/ )
 - http://juniperphoton.net/
 - [justinxinliu](https://stackoverflow.com/users/231837/justin-xl) 堆栈网25k大神
 - [Ben Cull's Blog](http://benjii.me/ ) asp 大神
 - [Kenny Kerr ](https://kennykerr.ca/ ) C++ WINRT 加拿大微软团队
